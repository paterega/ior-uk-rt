//
//  DetailsViewControllerTests.swift
//  Recruitment-iOSTests
//
//  Created by Yuriy Paterega on 6/4/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit
import Foundation
import XCTest
@testable import Recruitment_iOS

class DetailsViewControllerTests: XCTestCase {
    
    var viewController: DetailsViewController!
    
    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController else  {
            XCTAssert(true, "VC not found")
            return
        }
        viewController = vc
        _ = viewController.view
    }
    
    override func tearDown() {
        viewController = nil
    }
    
    func testTitleName() {
        XCTAssertEqual(viewController.setTitleStyle("Untitled"), "UnTiTlEd")
        XCTAssertNotEqual(viewController.setTitleStyle("Item1"), "Item1")
    }
}
