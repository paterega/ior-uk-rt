//
//  TableViewControllerTests.swift
//  Recruitment-iOSTests
//
//  Created by Yuriy Paterega on 6/4/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit
import Foundation
import XCTest
@testable import Recruitment_iOS

class TableViewControllerTests: XCTestCase {
    
    var viewController: TableViewController!
    
    class FakeNetworkManager: NetworkingManager {
        var downloadItemsCalled = false
        var result = [ItemModel(dictionary: [:])]
        
        override func downloadItems(_ completion: @escaping (_ items: [ItemModel]?,_ error: NetworkError?)->()) {
            downloadItemsCalled = true
            completion(result, nil)
        }
    }
    
    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "TableViewController") as? TableViewController else  {
            XCTAssert(true, "VC not found")
            return
        }
        viewController = vc
        _ = viewController.view
    }
    
    override func tearDown() {
        viewController = nil
    }
    
    func test_fetchData() {
        let fakeNetworkManager = FakeNetworkManager()
        
        viewController.makeNetworkRequest(fakeNetworkManager)
        
        XCTAssertTrue(fakeNetworkManager.downloadItemsCalled, "Network service return result")
        
        let viewControllersItemsCount = viewController.returnDownloadedData().count
        let downloadedItemsCount = fakeNetworkManager.result.count
        
        XCTAssertEqual(viewControllersItemsCount, downloadedItemsCount, "number of elements must be same")
    }
    
}
