//
//  DetailsViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    
    var item: TransitionDetailsModel?
    
    override func viewDidLoad() {
        setupData()
    }
    
    fileprivate func setupData() {
        if let item = item {
            view.backgroundColor = item.color
            getItemDetailsData(item.itemId)
            self.title = setTitleStyle(item.title)
        }
    }

    func setTitleStyle(_ title: String) -> String {
        return title.enumerated().map { $0.offset % 2 == 0 ? String($0.element).uppercased() : String($0.element) }.joined()
    }
    
    func getItemDetailsData(_ itemID: String, networkManager: NetworkingManager = NetworkingManager()) {
        networkManager.downloadItemWithID(itemID, completion: {[weak self] item, error in
            if let item = item {
                self?.setScreenStyle(for: item)
                return
            }
            print("wrong item id")
        })
    }
    
    fileprivate func setScreenStyle(for item: ItemDetailsModel) {
        textView.text = item.attributes.desc
    }
    
}
