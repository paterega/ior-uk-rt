//
//  CustomTableViewCell.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 6/4/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    
}
