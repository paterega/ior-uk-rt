//
//  TableViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

private struct Confuguration {
    let segueIdentifier = "TableViewSegue"
    let cellID = "CustomCell"
    let estimatedRowHeight: CGFloat = 100
}

class TableViewController: UITableViewController {
    
    private var itemModels:[ItemModel] = []
    private var itemID = ""
    private var config = Confuguration()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTableViewParameters()
        makeNetworkRequest()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == config.segueIdentifier {
            if let viewController = segue.destination as? DetailsViewController, let row = tableView.indexPathForSelectedRow?.row {
                viewController.item = TransitionDetailsModel(itemId: itemModels[row].id, color: itemModels[row].attributes.color, title: itemModels[row].attributes.name)
            }
        }
    }
}

extension TableViewController {
    func makeNetworkRequest(_ networkManager: NetworkingManager = NetworkingManager()) {
       networkManager.downloadItems { [weak self] items, error in
            if let items = items {
                self?.itemModels = items
                self?.tableView.reloadData()
            }
        }
    }
    
    func returnDownloadedData() -> [ItemModel] {
        return itemModels
    }
}

extension TableViewController {
    
    fileprivate func setTableViewParameters() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = config.estimatedRowHeight
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: config.cellID, for: indexPath) as? CustomTableViewCell else { fatalError("Cant deque cell") }
        let itemModel = itemModels[indexPath.row]
        cell.backgroundColor = itemModel.attributes.color
        cell.title.text = itemModel.attributes.name
        cell.subtitle.text = itemModel.attributes.preview
        return cell
    }
}
