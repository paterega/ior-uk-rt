//
//  NetworkingManager.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

public enum NetworkError: String, Error {
    case wrongDictionaryData = "Dictionary data is empty"
    case badParameters = "Initial parameters are bad"
}

class NetworkingManager {
    
    func downloadItems(_ completion: @escaping (_ items: [ItemModel]?,_ error: NetworkError?)->()) {
        request(filename: "Items.json") { dictionary in
            if let values = dictionary["data"] as? Array<Dictionary<String, AnyObject>> {
                let result = values.map { ItemModel(dictionary: $0) }
                completion(result,nil)
                return
            }
            completion(nil,.wrongDictionaryData)
        }
    }
    
    func downloadItemWithID(_ id:String, completion: @escaping (_ item: ItemDetailsModel?,_ error: NetworkError?)->()) {
        
        if id.isEmpty {
            completion(nil, .badParameters)
            return
        }
        
        request(filename: "Item\(id).json") { dictionary in
            if let value = dictionary["data"] as? Dictionary<String, AnyObject> {
                let result = ItemDetailsModel(dictionary: value)
                completion(result,nil)
                return
            }
            completion(nil, .wrongDictionaryData)
        }
    }
    
}

// MARK: Non-changing
extension NetworkingManager {
    private func request(filename:String, completionBlock:@escaping (Dictionary<String, AnyObject>) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let dictionary = JSONParser.jsonFromFilename(filename) {
                completionBlock(dictionary)
            } else {
                completionBlock([:])
            }
        }
    }
}

