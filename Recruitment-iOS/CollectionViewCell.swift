//
//  CollectionViewCell.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 6/3/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        title.text = nil
    
    }
}
