//
//  CollectionViewController.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 6/3/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

private struct Confuguration {
    let segueIdentifier = "CollectionViewSegue"
    let collectionViewCellId = "CustomCollectionViewCell"
    let cornerRadius: CGFloat = 10
    let padding: CGFloat = 25
}
class CollectionViewController: UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    
    private var config = Confuguration()
    var itemModels:[ItemModel] = []
    
    override func viewDidLoad() {
        setCollectionViewDelegate()
        makeNetworkRequest()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == config.segueIdentifier {
            if let viewController = segue.destination as? DetailsViewController, let index = collectionView.indexPathsForSelectedItems?.first {
                viewController.item = TransitionDetailsModel(itemId: itemModels[index.row].id, color: itemModels[index.row].attributes.color, title: itemModels[index.row].attributes.name)
            }
        }
    }
}

extension CollectionViewController {
    fileprivate func makeNetworkRequest(_ networkManager: NetworkingManager = NetworkingManager()) {
        networkManager.downloadItems {[weak self]  items, error in
            if let items = items {
                self?.itemModels = items
                self?.collectionView.reloadData()
            }
        }
    }
    
    fileprivate func setCollectionViewDelegate() {
        collectionView.delegate = self
        collectionView.dataSource = self
    }
}

extension CollectionViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemModels.count
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: config.collectionViewCellId, for: indexPath) as? CollectionViewCell else { fatalError("cant deque cell") }
        cell.title.text = itemModels[indexPath.row].attributes.name
        cell.backgroundColor = itemModels[indexPath.row].attributes.color
        return cell
    }
}

extension CollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionCellSize = collectionView.frame.size.width - config.padding
        return CGSize(width: collectionCellSize/2, height: collectionCellSize/2)
        
    }
}

extension CollectionViewController: UICollectionViewDataSource {

}
