//
//  ItemDetailsModel.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class ItemDetailsModel {
    
    var id: String
    var type: ItemModelType
    var attributes: ItemDetailsModelAttributes
    
    init(dictionary: [String: Any]) {
        self.id = dictionary["id"] as? String ?? ""
        self.type = ItemModelType(rawValue: dictionary["type"] as? String ?? "") ?? .unknown
        self.attributes = ItemDetailsModelAttributes(dictionary: dictionary["attributes"] as? [String:Any] ?? [:])
    }
}

struct ItemDetailsModelAttributes {
    var name: String
    var desc: String
    var color: UIColor
    
    init(dictionary: [String: Any]) {
        self.name = dictionary["name"] as? String ?? ""
        self.desc = dictionary["desc"] as? String ?? ""
        self.color = Color(rawValue: dictionary["color"] as? String ?? "")?.create ?? UIColor.black
    }
}
