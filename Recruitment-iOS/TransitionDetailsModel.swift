//
//  TransitionDetailsModel.swift
//  Recruitment-iOS
//
//  Created by Yuriy Paterega on 6/4/19.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

struct TransitionDetailsModel {
    var itemId = "1"
    var color = UIColor.black
    var title = ""
    
    init(itemId: String, color: UIColor, title: String) {
        self.itemId = itemId
        self.color = color
        self.title = title
    }
}
