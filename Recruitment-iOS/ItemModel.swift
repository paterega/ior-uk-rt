//
//  ItemModel.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class ItemModel {

    var id: String
    var type: ItemModelType
    var attributes: ItemModelAttributes

    init(dictionary: [String: Any]) {
        self.id = dictionary["id"] as? String ?? ""
        self.type = ItemModelType(rawValue: dictionary["type"] as? String ?? "") ?? .unknown
        self.attributes = ItemModelAttributes(dictionary: dictionary["attributes"] as? [String:Any] ?? [:])
    }
}

struct ItemModelAttributes {
    var name: String
    var preview: String
    var color: UIColor
    
    init(dictionary: [String: Any]) {
        self.name = dictionary["name"] as? String ?? ""
        self.preview = dictionary["preview"] as? String ?? ""
        self.color = Color(rawValue: dictionary["color"] as? String ?? "")?.create ?? UIColor.black
    }
    
}

enum ItemModelType: String {
    case item = "Items"
    case itemDetails = "ItemDetails"
    case unknown = ""
}

enum Color: String {
    case red = "Red"
    case blue = "Blue"
    case green = "Green"
    case yellow = "Yellow"
    case purple = "Purple"
    case black = ""
    
    var create: UIColor {
        switch self {
        case .red:
            return UIColor.red
        case .blue:
            return UIColor.blue
        case .green:
            return UIColor.green
        case .yellow:
            return UIColor.yellow
        case .purple:
            return UIColor.purple
        case .black:
            return UIColor.black
        }
    }
}

